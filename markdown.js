class MarkdownFlavor {
  constructor(
    name,
    codeBlockContents,
    postCodeBlock,
    horizontalRule,
    blockQuote,
    paragraph,
    list,
    listItem,
    image,
    link,
    italics,
    bold,
    headerOne,
    headerTwo,
    headerThree
  ) {
    this.name = name;
    this.codeBlockContents = codeBlockContents;
    this.postCodeBlock = postCodeBlock;
    this.horizontalRule = horizontalRule;
    this.blockQuote = blockQuote;
    this.paragraph = paragraph;
    this.list = list;
    this.listItem = listItem;
    this.image = image;
    this.link = link;
    this.italics = italics;
    this.bold = bold;
    this.headerOne = headerOne;
    this.headerTwo = headerTwo;
    this.headerThree = headerThree;
  }
}

var markdownFlavors = [
  new MarkdownFlavor(
    "github",
    new RegExp(/`{3}([^`]+)`{3}/, "g"),
    new RegExp(/`{6}/, "g"),
    new RegExp(/^(-{3}|\*{3}|\={3})$/, "gm"),
    new RegExp(/^>(.+\n?)+\n?/, "gm"),
    new RegExp(/^(?:(?=[^#-<])([ ]?.+)\n?)+?\n*/, "gm"),
    new RegExp(/((?:- .+(?:\n|\r\n)?)+)/, "gm"),
    new RegExp(/- (.+)/, "gm"),
    new RegExp(/!\[(.+?)\]\(((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)\)/, "g"),
    new RegExp(/\[(.+?)\]\(((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)\)/, "g"),
    new RegExp(/\*{1}([\w\s\n]+[^\*]+)\*{1}/, "g"),
    new RegExp(/\*{2}([\w\s\n]+[^\*]+)\*{2}/, "g"),
    new RegExp(/^(?:[ ]{0,}# )(.+?)$/, "gm"),
    new RegExp(/^(?:[ ]{0,}## )(.+?)$/, "gm"),
    new RegExp(/^(?:[ ]{0,}### )(.+?)$/, "gm")
  )
];

class MdJs {
  parseTag(regex, string, replace) {
    if (string.match(regex) !== null) {
      string = string.replace(regex, replace);
    }
    return string;
  }
  parse(flavorName, string) {
    var mdflavor = markdownFlavors.find(flavor => {
      return flavor.name === flavorName;
    });
    var codeBlockContents = string.match(mdflavor.codeBlockContents);
    string = string.replace(mdflavor.codeBlockContents, "``````");
    string = this.parseTag(mdflavor.horizontalRule, string, "<hr>");

    string = this.parseTag(mdflavor.blockQuote, string, "<blockquote>$1</blockquote>");
    string = this.parseTag(mdflavor.paragraph, string, "<p>$1</p>");

    string = this.parseTag(mdflavor.list, string, "<ul>$1</ul>");
    string = this.parseTag(mdflavor.listItem, string, "<li>$1</li>");

    string = this.parseTag(mdflavor.image, string, "<img src='$2' alt='$1'>");
    string = this.parseTag(mdflavor.link, string, "<a href='$2'>$1</a>");
    string = string.replace(mdflavor.bold, "<strong>$1</strong>");
    string = string.replace(mdflavor.italics, "<em>$1</em>");
    string = string.replace(mdflavor.headerThree, "<h3>$1</h3>");
    string = string.replace(mdflavor.headerTwo, "<h2>$1</h2>");
    string = string.replace(mdflavor.headerOne, "<h1>$1</h1>");


    if (codeBlockContents != null) {
      codeBlockContents.forEach(contents => {
        string = string.replace(
          mdflavor.postCodeBlock,
          "<pre><code>" + contents.substring(4, contents.length - 3) + "</code></pre>"
        );
      });
    }
    return string;
  }


}

var mdjs = new MdJs();
